# Git practice

Hello, this is git practice repository. Please feel free to check it out before more content will be added to the Bitbucket. 
Please find below list of commands used in this repository (some of them were used locally only of course :)

git clone
git branch
git branch <branch-name>
git checkout -b <branch-name>
git checkout <branch-name>
git add <file-name> or <.>
git commit -am <"Comment">
git pull <origin branch-name>
git push <origin branch-name>
git branch -d <branch-name>
git branch -D <branch-name>
git checkout <file-name>
git reset HEAD <file-name> or <~1>
git log 
git log --oneline
git revert <commit-number>